<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Department;
use App\Candidate;
use App\Role;
use App\Userrole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Collection;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all(); 
        $departments = Department::all();   
        $roles = Role::all();
        return view('users.index', compact('users','departments','roles'));
    }

    public function changeDepartment($uid,$did){
        $user = User::findOrFail($uid);
        if($user->candidates->count() == 0) {
            $user->department_id = $did;
            $user->save();
        }
        else {
            Session::flash('notallowed', 'You cannot change department to this user because he has candidates');
        }
        return back();
    }

    public function makeManager($uid){
        if(Auth::user()->isAdmin()){
            $userrole = new Userrole();
            $userrole->user_id = $uid;
            $userrole->role_id = 2;
            $userrole->save();
            Session::flash('message','user is now manager');
        }
        return redirect('users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('users.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->department_id = $request->department_id;
        $user->save();
        return redirect('candidates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', compact('user','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required'
        ]);
        $user = User::findOrFail($id);
        $user->update($request->all());
        return redirect('users'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('delete-user');
        $user = User::findOrFail($id);
        $candidates = Candidate::where('user_id','=',$id)->update(['user_id'=>null]);
        $user->delete(); 
        return redirect('users');
    }

    public function userDetails($id)
    {
        $user = User::findOrFail($id); 
        $departments = Department::all();      
        return view('users.details', compact('user','departments'));
    }
}
