@extends('layouts.app')

@section('title','User list')

@section('content')
    @if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
        @endif
        <h1>Users List</h1>
        @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
        @endif
        <table class="table table-striped">
            <tr>
                <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Roles</th><th>Make Manager</th><th>Created</th><th>Updated</th><th>Details</th><th>Delete</th>
            </tr>
            <!-- table data -->
            @foreach($users as $user)
                <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->departments->name}}</td>
                <td>
                    @foreach ($user->roles as $role)
                        {{ $role->name }}
                    @endforeach
                </td>
                <td>
                @if(!$user->isManager() )
                <a href = "{{route('user.makemanager',$user->id)}}" class="btn btn-primary">Make Manager</a>
                @endif
                @if($user->isManager() )
                <a href = "{{route('userroles.delete',$user->id)}} " class="btn btn-primary">Cancel Manager</a>
                @endif
                </td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td><a href = "{{route('user.userdetails', $user->id)}}" class="btn btn-primary">Details</a></td>
                <td>
                <a href = "{{route('user.delete',$user->id)}}" class="btn btn-primary">Delete</a>
                </td>
                </tr>
            @endforeach
    </table>
@endsection