@extends('layouts.app')

@section('title', 'User Details')

@section('content')  
    @if(Session::has('notallowed'))
    <div class='alert alert-danger'>
        {{Session::get('notallowed')}}
    </div>
    @endif     
       <h1>User Details</h1>
        <form method = "post" action = "{{action('UsersController@update',$user->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label>User Name : {{$user->name}}</label>
        </div>     
        <div class="form-group">
            <label>User Email : {{$user->email}}</label>
        </div>
        @if(!Auth::guest())
        @if(Auth::user()->isAdmin())  
        <div class="col-md-6">
            <select class="form-control" name="department_id">
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach($departments as $department)
                <option value="{{ $department->id }}">
                    {{ $department->name }}
                </option>
              @endforeach
            </div>
        </select>
         <a class="btn btn-primary" href="{{route('user.changedepartment',[$user->id,$department->id])}}"> Change Department </a></td>
        </div>
        </div>
        @endif
        @else
        <div class="form-group">
            <label>User Department : {{$user->departments->name}}</label>
        </div>
        @endif
        </form>
    </body>
</html>
@endsection